# Burkhard Lück <lueck@hube-lueck.de>, 2014.
# Frederik Schwarzer <schwarzer@kde.org>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-04-25 00:18+0000\n"
"PO-Revision-Date: 2016-11-22 20:53+0100\n"
"Last-Translator: Frederik Schwarzer <schwarzer@kde.org>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: OkularOdtGenerator.cpp:118
#, kde-format
msgid "Language"
msgstr "Sprache"

#~ msgctxt "(qtundo-format)"
#~ msgid "Language"
#~ msgstr "Sprache"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Burkhard Lück"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "lueck@hube-lueck.de"

#~ msgid "ODT/OTT Backend"
#~ msgstr "ODT/OTT-Backend"

#~ msgid "ODT/OTT file renderer"
#~ msgstr "ODT/OTT-Datei-Renderer"

#~ msgid "© 2012 Sven Langkamp"
#~ msgstr "© 2012 Sven Langkamp"
